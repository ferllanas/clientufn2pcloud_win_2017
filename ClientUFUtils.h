//
//  ClientUFUtils.hpp
//  ClientUF
//
//  Created by Fernando Llanas on 05/12/17.
//
//

#ifndef __ClientUFUtils_H_DEFINED__
#define __ClientUFUtils_H_DEFINED__


#include"IPMUnknown.h"




class IClientUFUtils:public IPMUnknown
{
public:
    enum { kDefaultIID = IID_IClientUFUtilsINTERFACE };
    
    
    virtual const bool16 P88WSUpladFile(IDFile fileToUpload,
                                           PMString URLWS,
                                           PMString publicacion,
                                           PMString documento,
                                           PMString usuario)=0;
    
    virtual const int32 filesCountOnDir(PMString dirPath, PMString URLWS) = 0;

    
    
};

#endif /* ClientUFUtils_h */
