//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa S.A de C.V
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================
/*
REGISTER_PMINTERFACE(ClientUFActionComponent, kClientUFActionComponentImpl)
REGISTER_PMINTERFACE(ClientUFDialogController, kClientUFDialogControllerImpl)
REGISTER_PMINTERFACE(ClientUFDialogObserver, kClientUFDialogObserverImpl)
*/
REGISTER_PMINTERFACE(ClientUFUtils, kClientUFUtilsImpl)

