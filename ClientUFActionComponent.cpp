//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa S.A de C.V
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "IPMStream.h"
#include "IXferBytes.h"

// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "StreamUtil.h"
#include "FileUtils.h"
#include "PDFVTUtils.h"
#include "SDKFileHelper.h"

#include "uploadwsdlBinding.nsmap"//"P88WebService/p88wsdlBinding.nsmap"
#include "soapuploadwsdlBindingProxy.h"//"P88WebService/soapp88wsdlBindingProxy.h"
#include "stdsoap2.h"
#include <queue>
#include <map>
// General includes:
#include "CActionComponent.h"
#include "CAlert.h"
//#include "apr_base64.h"

// Project includes:
#include "ClientUFID.h"

using namespace std;
/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup clientuf

*/
class ClientUFActionComponent : public CActionComponent
{
public:
/**
 Constructor.
 @param boss interface ptr from boss object on which this interface is aggregated.
 */
		ClientUFActionComponent(IPMUnknown* boss);

		/** The action component should perform the requested action.
			This is where the menu item's action is taken.
			When a menu item is selected, the Menu Manager determines
			which plug-in is responsible for it, and calls its DoAction
			with the ID for the menu item chosen.

			@param actionID identifies the menu item that was selected.
			@param ac active context
			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
			@param widget contains the widget that invoked this action. May be nil. 
			*/
		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);

	private:
		/** Encapsulates functionality for the about menu item. */
		void DoAbout();
		
		/** Opens this plug-in's dialog. */
		void DoDialog();
    

		


};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(ClientUFActionComponent, kClientUFActionComponentImpl)

/* ClientUFActionComponent Constructor
*/
ClientUFActionComponent::ClientUFActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* DoAction
*/
void ClientUFActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	switch (actionID.Get())
	{

		case kClientUFPopupAboutThisActionID:
		case kClientUFAboutActionID:
		{
			this->DoAbout();
			break;
		}
					

		case kClientUFDialogActionID:
		{
			this->DoDialog();
			break;
		}


		default:
		{
			break;
		}
	}
}

/* DoAbout
*/
void ClientUFActionComponent::DoAbout()
{
    
    /*IDFile fileFolder;
    PMString Archivo="";
    SDKFileOpenChooser fileChooser;
    fileChooser.SetTitle("Select XMP MetaData file to replace with");
    fileChooser.ShowDialog();
	if (fileChooser.IsChosen())
    {
        fileFolder = fileChooser.GetIDFile();
    }

    if (fileChooser.IsExisting() == kFalse)
    {
          Archivo = fileChooser.GetPath();
    }
    //FileUtils::PMStringToIDFile(Archivo,fileFolder);

    InterfacePtr<IPMStream> stream(StreamUtil::CreateFileStreamRead(fileFolder));
    if(stream)
    {
        stream->Seek(0, kSeekFromStart);
        
        int32 count = 0;
        
        //
        while (stream->GetStreamState() == kStreamStateGood)
        {
            uchar tmp;
            stream->XferByte(tmp);
            if (stream->GetStreamState() == kStreamStateEOF)
            {
                break;
            }
            count++;
        }
        
        PMString outStr;
        uchar* fPointerStreamBuffer = new uchar[count];
        stream->Seek(0, kSeekFromStart);
        int32 index = 0;
        while (stream->GetStreamState() == kStreamStateGood)
        {
            uchar tmp;
            stream->XferByte(tmp);
            if (stream->GetStreamState() == kStreamStateEOF)
            {
                break;
            }
            outStr.Append((char)tmp);
            fPointerStreamBuffer [index++] = tmp;
        }
        stream->Close();
        //stream->Release();
       
        uploadwsdlBindingProxy p88wsdlBProxy(SOAP_C_UTFSTRING);
        p88wsdlBProxy.soap_endpoint="http://localhost/assembler/WSUploadFile/WSUploadFile.php?wsdl";//"http://localhost/p88appleiPadv17/WSUploadFile.php";
    
        // std::string str((char*)fPointerStreamBuffer);
        //std::string str(outStr.GetPlatformString());
        //std::string str(  base64_encode ( (char *) outStr.GetUTF8String().c_str(), count));
         std::string str(  base64_encode ( (char *) fPointerStreamBuffer, count));
        std::string response;
    
        //std::string file, std::string namefile, std::string issue, std::string documentoid, std::string username, std::string &return_
        p88wsdlBProxy.upload_USCOREfile(str, fileFolder.GetFileName().GetUTF8String().c_str(),"1390-ITrueque", "15","ITrueque" , response);//(emailIn, response);
        
        if(p88wsdlBProxy.error){
            p88wsdlBProxy.soap_stream_fault(std::cerr);
            
            
            PMString ErrorS=p88wsdlBProxy.soap_fault_string();
            if(ErrorS.Contains("404 Not Found"))
            {
                CAlert::InformationAlert("404 Not Found ");
            }
            else {
                PMString myerror = "";
                myerror.Append(". Error: ");
                myerror.Append(p88wsdlBProxy.soap_fault_string());
                myerror.Append(". Detalle: ");
                myerror.Append(p88wsdlBProxy.soap_fault_detail());
                CAlert::InformationAlert(myerror);
            }
        }
        else
        {
            WideString mycdr(response.c_str());
            CAlert::InformationAlert("OK "+PMString(mycdr));
        }
        
    }*/
    CAlert::ModalAlert
    (
     kClientUFAboutBoxStringKey,				// Alert string
     kOKString, 						// OK button
     kNullString, 						// No second button
     kNullString, 						// No third button
     1,							// Set OK button to default
     CAlert::eInformationIcon				// Information icon.
     );
}



/* DoDialog
*/
void ClientUFActionComponent::DoDialog()
{
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kClientUFPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kSDKDefDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		ASSERT(dialog);
		if (dialog == nil) {
			break;
		}

		// Open the dialog.
		dialog->Open(); 
	
	} while (false);			
}

