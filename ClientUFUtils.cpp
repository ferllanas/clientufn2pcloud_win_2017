//
//  ClientUFUtils.cpp
//  ClientUF
//
//  Created by Fernando Llanas on 05/12/17.
//
//

#include "VCPlugInHeaders.h"


// Interface includes:
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "IPMStream.h"
#include "IXferBytes.h"

// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "StreamUtil.h"
#include "FileUtils.h"
#include "PDFVTUtils.h"
#include "SDKFileHelper.h"

#include "uploadwsdlBinding.nsmap"//"P88WebService/p88wsdlBinding.nsmap"
#include "soapuploadwsdlBindingProxy.h"//"P88WebService/soapp88wsdlBindingProxy.h"
#include "stdsoap2.h"
#include <queue>
#include <map>
// General includes:
#include "CActionComponent.h"
#include "CAlert.h"
//#include "apr_base64.h"

// Project includes:
#include "ClientUFID.h"
#include "ClientUFUtils.h"
#include "InterlasaUtilities.h"

using namespace std;

class ClientUFUtils : public CPMUnknown<IClientUFUtils>
{
public:
    /** Constructor.
     param boss boss object on which this interface is aggregated.
     */
    ClientUFUtils (IPMUnknown *boss);
    
    const bool16 P88WSUpladFile(IDFile fileToUpload,
                                PMString URLWS,
                                PMString publicacion,
                                PMString documento,
                                PMString usuario);
    
    const int32 filesCountOnDir(PMString dirPath, PMString URLWS);
    
    
    
private:
    std::string base64_encode(char const* bytes_to_encode, unsigned int in_len);
    
};
/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
 */
CREATE_PMINTERFACE(ClientUFUtils, kClientUFUtilsImpl)


/* HelloWorld Constructor
 */
ClientUFUtils::ClientUFUtils(IPMUnknown* boss)
: CPMUnknown<IClientUFUtils>(boss)
{
    // do nothing.
    
    /* NOTE:
     There used to be code that would set the preference value by default.
     That was removed in favor of the workspace responders.
     
     The responders get called, and they will try to setup the value
     for this custom preference.  It is one of the responders that will
     set the correct initial value for the cached preference, fPrefState.
     
     If the global workspace is already created from a previous
     application session, the ReadWrite() method will set the
     cached preference, fPrefState. The same ReadWrite() will be called when
     an existing document is opened.
     
     Refer to CstPrfNewWSResponder.cpp and N2PSQLUtils.cpp
     for details on how to set the workspace defaults.
     */
}

const int32 ClientUFUtils::filesCountOnDir(PMString dirPath, PMString URLWS){
    
    int32 retval=0;
    
    do{
        
        
        
       PMString  pathFile = *dirPath.GetItem("apaginas", 2);
        // pathFile = "apaginas"+pathFile;
        pathFile = InterlasaUtilities::MacToUnix(pathFile,kFalse);
        
        
        
        uploadwsdlBindingProxy p88wsdlBProxy(SOAP_C_UTFSTRING);
        
        std::string	resultString = URLWS.GetPlatformString ();
        p88wsdlBProxy.soap_endpoint = resultString.c_str();
        
       // std::string str(  base64_encode ( (char *) fPointerStreamBuffer, count));
       std::string response;
        
        PMString XSW="pathFile: "+pathFile;
        //CAlert::InformationAlert(XSW+" "+ URLWS);
        
        p88wsdlBProxy.fileCountOnDir(pathFile.GetUTF8String().c_str(), response);
        if(p88wsdlBProxy.error){
            p88wsdlBProxy.soap_stream_fault(std::cerr);
            
            
            PMString ErrorS=p88wsdlBProxy.soap_fault_string();
            if(ErrorS.Contains("404 Not Found"))
            {
                CAlert::InformationAlert("404 Not Found ");
            }
            else {
                PMString myerror = "CCC ";
                myerror.Append(". Error: ");
                myerror.Append(p88wsdlBProxy.soap_fault_string());
                myerror.Append(". Detalle: ");
                myerror.Append(p88wsdlBProxy.soap_fault_detail());
                myerror.Append("; ");
                myerror.Append(pathFile);
                CAlert::InformationAlert(myerror);
            }
        }
        else
        {
            XSW="";
            XSW.Append(response.c_str());
            //CAlert::InformationAlert(XSW);
            retval = XSW.GetAsNumber();
           
        }
    }while(false);
    return retval;
    
}


const bool16 ClientUFUtils::P88WSUpladFile(IDFile fileToUpload,
                      PMString URLWS,
                      PMString publicacion,
                      PMString documento,
                      PMString usuario)
{
    bool16 retval=kFalse;
    
    do{
        
		
		
#ifdef WINDOWS

		/*PMString cadenaFile = "C:\\sendfile.jpg";
		IDFile dataSourceFile = FileUtils::PMStringToSysFile(cadenaFile);
		if (!FileUtils::CopyFile(fileToUpload, dataSourceFile)) {
			CAlert::InformationAlert("No pudo hacer la copia");
			break;
		}*/
		InterfacePtr<IPMStream> stream(StreamUtil::CreateFileStreamRead(fileToUpload, kOpenIn));
#else
		InterfacePtr<IPMStream> stream(StreamUtil::CreateFileStreamRead(fileToUpload, kOpenIn));
#endif
       
		if (stream == nil) {
			CAlert::InformationAlert("No pudo Leer");
			break;
		}
		else
        {
            stream->Seek(0, kSeekFromStart);
            
            int32 count = 0;
            
            //
            while (stream->GetStreamState() == kStreamStateGood)
            {
                uchar tmp;
                stream->XferByte(tmp);
                if (stream->GetStreamState() == kStreamStateEOF)
                {
                    break;
                }
                count++;
            }
            
            PMString outStr;
            uchar* fPointerStreamBuffer = new uchar[count];
            stream->Seek(0, kSeekFromStart);
            int32 index = 0;
            while (stream->GetStreamState() == kStreamStateGood)
            {
                uchar tmp;
                stream->XferByte(tmp);
                if (stream->GetStreamState() == kStreamStateEOF)
                {
                    break;
                }
                outStr.Append((char)tmp);
                fPointerStreamBuffer [index++] = tmp;
            }
            stream->Close();
            
            uploadwsdlBindingProxy p88wsdlBProxy(SOAP_C_UTFSTRING);
            
            std::string	resultString = URLWS.GetPlatformString ();
            p88wsdlBProxy.soap_endpoint = resultString.c_str();

            //"http://localhost/assembler/WSUploadFile/WSUploadFile.php?wsdl";
            //"http://localhost/p88appleiPadv17/WSUploadFile.php";
            

            std::string str(  base64_encode ( (char *) fPointerStreamBuffer, count));
            std::string response;
            
            PMString pathFile;
            FileUtils::IDFileToPMString(fileToUpload, pathFile);
            
            pathFile = *pathFile.GetItem("apaginas", 2);
            
             std::string pathFileStr = pathFile.GetUTF8String();
            // pathFile = "apaginas"+pathFile;
            //pathFile = InterlasaUtilities::MacToUnix(pathFile,kFalse);
            //CAlert::InformationAlert(pathFile);
            p88wsdlBProxy.upload_USCOREfile(str,
                                            (char *)pathFileStr.c_str(),
                                            publicacion.GetUTF8String().c_str(),
                                            documento.GetUTF8String().c_str(),
                                            usuario.GetUTF8String().c_str() ,
                                            response);//(emailIn, response);
            
            if(p88wsdlBProxy.error){
                p88wsdlBProxy.soap_stream_fault(std::cerr);
                
                
                PMString ErrorS=p88wsdlBProxy.soap_fault_string();
                if(ErrorS.Contains("404 Not Found"))
                {
                    CAlert::InformationAlert("404 Not Found ");
                }
                else {
                    PMString myerror = "ZZZ ";
                    myerror.Append(". Error: ");
                    myerror.Append(p88wsdlBProxy.soap_fault_string());
                    myerror.Append(". Detalle: ");
                    myerror.Append(p88wsdlBProxy.soap_fault_detail());
                    CAlert::InformationAlert(myerror);
                }
            }
            else
            {
                PMString mycdr(response.c_str());
                if(PMString("OK")==mycdr){
                    retval=kTrue;
                }
                else{
                    CAlert::InformationAlert(PMString(mycdr));
                }
            }
        }
        
    }while(false);
    return retval;

}

static const std::string base64_chars =
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz"
"0123456789+/";
    
std::string ClientUFUtils::base64_encode(char const* bytes_to_encode, unsigned int in_len)
{
        std::string ret;
        int i = 0;
        int j = 0;
        unsigned char char_array_3[3];
        unsigned char char_array_4[4];
        
        while (in_len--) {
            char_array_3[i++] = *(bytes_to_encode++);
            if (i == 3) {
                char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
                char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
                char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
                char_array_4[3] = char_array_3[2] & 0x3f;
                
                for(i = 0; (i <4) ; i++)
                    ret += base64_chars[char_array_4[i]];
                i = 0;
            }
        }
        
        if (i)
        {
            for(j = i; j < 3; j++)
                char_array_3[j] = '\0';
            
            char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
            char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
            char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
            char_array_4[3] = char_array_3[2] & 0x3f;
            
            for (j = 0; (j < i + 1); j++)
                ret += base64_chars[char_array_4[j]];
            
            while((i++ < 3))
                ret += '=';
            
        }
        
        return ret;
        
    }