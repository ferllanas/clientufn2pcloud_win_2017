//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa S.A de C.V
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __ClientUFID_h__
#define __ClientUFID_h__

#include "SDKDef.h"

// Company:
#define kClientUFCompanyKey	"Publish88"		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kClientUFCompanyValue	"Publish88"		// Company name displayed externally.

// Plug-in:
#define kClientUFPluginName	"ClientUF"			// Name of this plug-in.
#define kClientUFPrefixNumber	0x17b400 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kClientUFVersion		"1.0"						// Version of this plug-in (for the About Box).
#define kClientUFAuthor		"Interlasa S.A de C.V"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kClientUFPrefixNumber above to modify the prefix.)
#define kClientUFPrefix		RezLong(kClientUFPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kClientUFStringPrefix	SDK_DEF_STRINGIZE(kClientUFPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kClientUFMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kClientUFMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kClientUFPluginID, kClientUFPrefix + 0)

// ClassIDs:
//DECLARE_PMID(kClassIDSpace, kClientUFActionComponentBoss, kClientUFPrefix + 0)
//DECLARE_PMID(kClassIDSpace, kClientUFPanelWidgetBoss, kClientUFPrefix + 1)
//DECLARE_PMID(kClassIDSpace, kClientUFDialogBoss, kClientUFPrefix + 2)
DECLARE_PMID(kClassIDSpace, kClientUFUtilsBoss, kClientUFPrefix + 3)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 4)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 5)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 6)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 7)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kClientUFBoss, kClientUFPrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IClientUFUtilsINTERFACE, kClientUFPrefix + 0)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTUFINTERFACE, kClientUFPrefix + 25)


// ImplementationIDs:
//DECLARE_PMID(kImplementationIDSpace, kClientUFActionComponentImpl, kClientUFPrefix + 0 )
//DECLARE_PMID(kImplementationIDSpace, kClientUFDialogControllerImpl, kClientUFPrefix + 1 )
//DECLARE_PMID(kImplementationIDSpace, kClientUFDialogObserverImpl, kClientUFPrefix + 2 )
DECLARE_PMID(kImplementationIDSpace, kClientUFUtilsImpl, kClientUFPrefix + 3)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 4)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 5)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kClientUFImpl, kClientUFPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kClientUFAboutActionID, kClientUFPrefix + 0)
DECLARE_PMID(kActionIDSpace, kClientUFPanelWidgetActionID, kClientUFPrefix + 1)
DECLARE_PMID(kActionIDSpace, kClientUFSeparator1ActionID, kClientUFPrefix + 2)
DECLARE_PMID(kActionIDSpace, kClientUFPopupAboutThisActionID, kClientUFPrefix + 3)
DECLARE_PMID(kActionIDSpace, kClientUFDialogActionID, kClientUFPrefix + 4)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kClientUFActionID, kClientUFPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kClientUFPanelWidgetID, kClientUFPrefix + 0)
DECLARE_PMID(kWidgetIDSpace, kClientUFDialogWidgetID, kClientUFPrefix + 1)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 2)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 3)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 4)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 5)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 6)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 7)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 8)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 9)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 10)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 11)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 13)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 18)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kClientUFWidgetID, kClientUFPrefix + 25)


// "About Plug-ins" sub-menu:
#define kClientUFAboutMenuKey			kClientUFStringPrefix "kClientUFAboutMenuKey"
#define kClientUFAboutMenuPath		kSDKDefStandardAboutMenuPath kClientUFCompanyKey

// "Plug-ins" sub-menu:
#define kClientUFPluginsMenuKey 		kClientUFStringPrefix "kClientUFPluginsMenuKey"
#define kClientUFPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kClientUFCompanyKey kSDKDefDelimitMenuPath kClientUFPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kClientUFAboutBoxStringKey	kClientUFStringPrefix "kClientUFAboutBoxStringKey"
#define kClientUFPanelTitleKey					kClientUFStringPrefix	"kClientUFPanelTitleKey"
#define kClientUFStaticTextKey kClientUFStringPrefix	"kClientUFStaticTextKey"
#define kClientUFInternalPopupMenuNameKey kClientUFStringPrefix	"kClientUFInternalPopupMenuNameKey"
#define kClientUFTargetMenuPath kClientUFInternalPopupMenuNameKey

// Menu item positions:

#define	kClientUFSeparator1MenuItemPosition		10.0
#define kClientUFAboutThisMenuItemPosition		11.0

#define kClientUFDialogTitleKey kClientUFStringPrefix "kClientUFDialogTitleKey"
// "Plug-ins" sub-menu item key for dialog:
#define kClientUFDialogMenuItemKey kClientUFStringPrefix "kClientUFDialogMenuItemKey"
// "Plug-ins" sub-menu item position for dialog:
#define kClientUFDialogMenuItemPosition	12.0


// Initial data format version numbers
#define kClientUFFirstMajorFormatNumber  RezLong(1)
#define kClientUFFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kClientUFCurrentMajorFormatNumber kClientUFFirstMajorFormatNumber
#define kClientUFCurrentMinorFormatNumber kClientUFFirstMinorFormatNumber

#endif // __ClientUFID_h__
